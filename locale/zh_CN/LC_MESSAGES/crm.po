#
msgid ""
msgstr ""
"Project-Id-Version: Odoo Business\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2015-09-06 01:43+0800\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../crm.rst:5
msgid "CRM"
msgstr "CRM"

#: ../../crm/salesteam.rst:5 ../../crm/salesteam/setup.rst:5
msgid "Sales Team"
msgstr "销售团队"

#: ../../crm/salesteam/setup/organize_pipeline.rst:3
msgid "Set up and organize your sales pipeline"
msgstr "建立和组织销售渠道"

#: ../../crm/salesteam/setup/organize_pipeline.rst:5
msgid ""
"A well structured sales pipeline is crucial in order to keep control of your"
" sales process and to have a 360-degrees view of your leads, opportunities "
"and customers."
msgstr "一个结构良好的销售渠道是为了保持你的销售过程的控制，并让您的线索，机会和客户提供360度的看法至关重要。"

#: ../../crm/salesteam/setup/organize_pipeline.rst:9
msgid ""
"The sales pipeline is a visual representation of your sales process, from "
"the first contact to the final sale. It refers to the process by which you "
"generate, qualify and close leads through your sales cycle. In Odoo CRM, "
"leads are brought in at the left end of the sales pipeline in the Kanban "
"view and then moved along to the right from one stage to another."
msgstr "销售渠道是销售过程的可视化表示，从第一次接触到最终销售。它是指由您生成，通过你的销售周期和资格接近线索的过程。在Odoo CRM中，引线在看板视图销售管道的左端带来然后移到沿向右从一个阶段到另一个。"

#: ../../crm/salesteam/setup/organize_pipeline.rst:16
msgid ""
"Each stage refers to a specific step in the sale cycle and specifically the "
"sale-readiness of your potential customer. The number of stages in the sales"
" funnel varies from one company to another. An example of a sales funnel "
"will contain the following stages: *Territory, Qualified, Qualified Sponsor,"
" Proposition, Negotiation, Won, Lost*."
msgstr "每个阶段是指在销售周期的特定步骤和专门出售就绪潜在客户的。阶段中的销售渠道中的数目从一个公司的不同而不同。销售漏斗的一个例子将包含以下几个阶段：*领地，合格的，合格的保荐人，命题，谈判，赢了，失去了*。"

#: ../../crm/salesteam/setup/organize_pipeline.rst:26
msgid ""
"Of course, each organization defines the sales funnel depending on their "
"processes and workflow, so more or fewer stages may exist."
msgstr "当然，每个组织定义销售漏斗取决于它们的操作和工作流程，因此更多或更少的阶段可以存在。"

#: ../../crm/salesteam/setup/organize_pipeline.rst:30
msgid "Create and organize your stages"
msgstr "创建和整理阶段"

#: ../../crm/salesteam/setup/organize_pipeline.rst:33
msgid "Add/ rearrange stages"
msgstr "添加/重新整理阶段"

#: ../../crm/salesteam/setup/organize_pipeline.rst:35
msgid ""
"From the sales module, go to your dashboard and click on the **PIPELINE** "
"button of the desired sales team. If you don't have any sales team yet, you "
"need to create one first."
msgstr "从销售模块，进入到仪表板，然后单击所需的销售团队的** **管道按钮。如果你没有任何的销售团队呢，你需要先创建一个。"

#: ../../crm/salesteam/setup/organize_pipeline.rst:46
msgid ""
"From the Kanban view of your pipeline, you can add stages by clicking on "
"**Add new column.** When a column is created, Odoo will then automatically "
"propose you to add another column in order to complete your process. If you "
"want to rearrange the order of your stages, you can easily do so by dragging"
" and dropping the column you want to move to the desired location."
msgstr "从您的管道的看板视图，您可以通过点击**添加新列添加阶段。**当创建一个列，Odoo将自动建议你添加另一列为了完成你的过程。如果你想重新排列阶段的顺序，可以通过拖放要移动到所需位置的列很容易做到这一点。"

#: ../../crm/salesteam/setup/organize_pipeline.rst:58
msgid ""
"You can add as many stages as you wish, even if we advise you not having "
"more than 6 in order to keep a clear pipeline"
msgstr "如你愿意，你可以添加尽可能多的阶段，即使我们劝你不必为了保持清醒的管道超过6"

#: ../../crm/salesteam/setup/organize_pipeline.rst:62
msgid "Activate the lead stage"
msgstr "激活铅阶段"

#: ../../crm/salesteam/setup/organize_pipeline.rst:64
msgid ""
"Some companies use a pre qualification step to manage their leads before to "
"convert them into opportunities. To activate the lead stage, go to "
":menuselection:`Configuration --> Settings` and select the radio button as "
"shown below. It will create a new submenu **Leads** under **Sales** that "
"gives you access to a listview of all your leads."
msgstr "一些公司使用预认证的步骤来管理自己的线之前，将它们转换成机会。要激活领先阶段，请访问：menuselection：`配置 - > Settings`并选择单选按钮，如下图所示。这将创建一个新的子菜单** **信息在销售** **，让您可以访问所有的线索列表视图。"

#: ../../crm/salesteam/setup/organize_pipeline.rst:74
msgid "Set up stage probabilities"
msgstr "设置舞台概率"

#: ../../crm/salesteam/setup/organize_pipeline.rst:77
msgid "What is a stage probability?"
msgstr "什么是一个阶段的概率有多大？"

#: ../../crm/salesteam/setup/organize_pipeline.rst:79
msgid ""
"To better understand what are the chances of closing a deal for a given "
"opportunity in your pipe, you have to set up a probability percentage for "
"each of your stages. That percentage refers to the success rate of closing "
"the deal."
msgstr "为了更好地了解什么是完成交易在你管一个给定的机会的机会，你必须建立一个概率百分比为您的每个阶段。这一比例是指完成交易的成功率。"

#: ../../crm/salesteam/setup/organize_pipeline.rst:84
msgid ""
"Setting up stage probabilities is essential if you want to estimate the "
"expected revenues of your sales cycle"
msgstr "如果你想要估计销售周期的预期收入建立阶段的概率是必不可少的"

#: ../../crm/salesteam/setup/organize_pipeline.rst:88
msgid ""
"For example, if your sales cycle contains the stages *Territory, Qualified, "
"Qualified Sponsor, Proposition, Negotiation, Won and Lost,* then your "
"workflow could look like this :"
msgstr "例如，如果你的销售周期包含阶段*领土合格的，合格的保荐人，命题，谈判，得而复失，*那么你的工作流程看起来是这样的："

#: ../../crm/salesteam/setup/organize_pipeline.rst:92
msgid ""
"**Territory** : opportunity just received from Leads Management or created "
"from a cold call campaign. Customer's Interest is not yet confirmed."
msgstr "**区域**：机会，刚刚从信息管理接收或创建从寒冷的呼叫活动。顾客的兴趣还没有被证实。"

#: ../../crm/salesteam/setup/organize_pipeline.rst:96
msgid "*Success rate : 5%*"
msgstr "*成功率：5％*"

#: ../../crm/salesteam/setup/organize_pipeline.rst:98
msgid ""
"**Qualified** : prospect's business and workflow are understood, pains are "
"identified and confirmed, budget and timing are known"
msgstr "** **合格：潜在客户的业务和工作流程的了解，痛苦的鉴定，确认，预算和时间是已知"

#: ../../crm/salesteam/setup/organize_pipeline.rst:101
msgid "*Success rate : 15%*"
msgstr "*成功率：15％*"

#: ../../crm/salesteam/setup/organize_pipeline.rst:103
msgid ""
"**Qualified sponsor**: direct contact with decision maker has been done"
msgstr "**合格发起人**：与决策者直接接触已完成"

#: ../../crm/salesteam/setup/organize_pipeline.rst:106
msgid "*Success rate : 25%*"
msgstr "*成功率：25％*"

#: ../../crm/salesteam/setup/organize_pipeline.rst:108
msgid "**Proposition** : the prospect received a quotation"
msgstr "**命题**：前景接到报价"

#: ../../crm/salesteam/setup/organize_pipeline.rst:110
msgid "*Success rate : 50%*"
msgstr "*成功率：50％*"

#: ../../crm/salesteam/setup/organize_pipeline.rst:112
msgid "**Negotiation**: the prospect negotiates his quotation"
msgstr "** **谈判：前景协商他的报价"

#: ../../crm/salesteam/setup/organize_pipeline.rst:114
msgid "*Success rate : 75%*"
msgstr "*成功率：75％*"

#: ../../crm/salesteam/setup/organize_pipeline.rst:116
msgid ""
"**Won** : the prospect confirmed his quotation and received a sales order. "
"He his now a customer"
msgstr "**荣获**：前景证实了他的报价，并获得了销售订单。他自己现在是一个客户"

#: ../../crm/salesteam/setup/organize_pipeline.rst:119
msgid "*Success rate : 100%*"
msgstr "*成功率：100％*"

#: ../../crm/salesteam/setup/organize_pipeline.rst:121
msgid "**Lost** : the prospect is no longer interested"
msgstr "** **失落：前景不再有兴趣"

#: ../../crm/salesteam/setup/organize_pipeline.rst:123
msgid "*Success rate : 0%*"
msgstr "*成功率：0％*"

#: ../../crm/salesteam/setup/organize_pipeline.rst:127
msgid ""
"Within your pipeline, each stage should correspond to a defined goal with a "
"corresponding probability. Every time you move your opportunity to the next "
"stage, your probability of closing the sale will automatically adapt."
msgstr "内的管道，每个阶段都对应一个限定目标用相应的概率。每次你移动你的机会来了下一个阶段，关闭销售会自动适应您的概率。"

#: ../../crm/salesteam/setup/organize_pipeline.rst:131
msgid ""
"You should consider using probability value as **100** when the deal is "
"closed-won and **0** for deal closed-lost."
msgstr "您应该考虑使用概率值** 100 **当交易结束，赢了，** 0 **为交易结束，丢失。"

#: ../../crm/salesteam/setup/organize_pipeline.rst:135
msgid "How to set up stage probabilities?"
msgstr "如何设置阶段的概率？"

#: ../../crm/salesteam/setup/organize_pipeline.rst:137
msgid ""
"To edit a stage, click on the **Settings** icon at the right of the desired "
"stage then on EDIT"
msgstr "要编辑阶段，请点击**设置**图标所需的舞台右边，然后在编辑"

#: ../../crm/salesteam/setup/organize_pipeline.rst:143
msgid ""
"Select the Change probability automatically checkbox to let Odoo adapt the "
"probability of the opportunity to the probability defined in the stage. For "
"example, if you set a probability of 0% (Lost) or 100% (Won), Odoo will "
"assign the corresponding stage when the opportunity is marked as Lost or "
"Won."
msgstr "自动选择切换概率复选框让Odoo适应的机会在舞台上定义的概率的概率。例如，如果设置为0％（丢失）或100％（韩元）的概率，Odoo将分配相应的阶段，当有机会被标记为丢失或韩元。"

#: ../../crm/salesteam/setup/organize_pipeline.rst:151
msgid ""
"Under the requirements field you can enter the internal requirements for "
"this stage. It will appear as a tooltip when you place your mouse over the "
"name of a stage."
msgstr "根据要求，现场就可以进入这一阶段的内在要求。当你将鼠标移到舞台的名字将出现一个提示。"

#: ../../crm/salesteam/setup/organize_pipeline.rst:163
msgid "Written by Geoffrey Bressan (Odoo)"
msgstr "写由杰弗里·布雷桑（Odoo）"

#: ../../crm/salesteam/setup/organize_pipeline.rst:164
msgid "Proofread by Samuel Cabodi (Odoo)"
msgstr "塞缪尔Cabodi校对（Odoo）"

